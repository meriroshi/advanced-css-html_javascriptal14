window.onload =  function () {
    var textCanvas = document.getElementById('text');
    var context = textCanvas.getContext("2d");

    context.font ="40px Tahoma";
    context.textAlign = "center";
    context.fillStyle = "blue";
    
    context.beginPath();
    context.fillText("This is my text", 350, 250);
}