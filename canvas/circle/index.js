window.onload = function() {
    var circleCanvas = document.getElementById('circle');
    var context = circleCanvas.getContext("2d");

    //filled circle
    // context.fillStyle = 'red';
    // context.beginPath();
    // context.arc(200, 200, 120, 0, Math.PI*2, true);
    // context.fill();

    //not filled circle
    context.strokeStyle = 'blue';
    context.beginPath();
    context.arc(300,120,120,0, Math.PI/2,true);
    context.stroke();

} 