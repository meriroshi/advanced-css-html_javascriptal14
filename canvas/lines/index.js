window.onload = function() {
    var canvasLine = document.getElementById("lines");
    var context = canvasLine.getContext("2d");
    context.lineWidth = 4;
    context.strokeStyle = "black";
    context.fillStyle = "black";
    
    context.beginPath(); //starts drawing
    
    context.moveTo(100, 100); //x=100, y=100
    context.lineTo(200,200); //first line
    context.lineTo(300,100); //second line

    context.fill();    //color the shape
    context.stroke();

    context.closePath(); //ends drawing
}