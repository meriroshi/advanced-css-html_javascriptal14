window.onload = function() {
    const canvasLoop = document.getElementById('loop');
    const context = canvasLoop.getContext("2d");

    var y = 200;
    var x = 10;
    var size =  40;

    for(let j=1; j<=10; j++){
        let red = 255 - 20*j;
        let green = 20*j;
        let blue = 10;
        let style = `rgb(${red}, ${green}, ${blue})`;
        
        context.fillStyle = style;
        context.fillRect(x,y, size, size);
        x += (size*1.5);
    }
}