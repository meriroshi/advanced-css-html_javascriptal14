window.onload = function () {
    var canvasRect = document.getElementById('rect');
    var context =  canvasRect.getContext("2d");

    context.strokeRect(50, 200, 50, 150);
    context.strokeStyle = 'red';

    context.fillRect(50,200,50,150);
    context.fillStyle = 'red';

    context.beginPath();
    context.stroke();
}