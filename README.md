How do you push an exciting project from your computer to Git?
<details><summary>Click to expand</summary>
1. Initialise a Git project by clicking 
-     New Project  
-     Create a blank project.
-     Set up a project name (advanced-css-html_javascriptAl14).
-     Click on Create project


2. Go back to the root folder of your project.

    > For this example, we will assume that the folder is located in the desktop folder, and the folder name is "advanced-css-html."

3. From the advanced-css-html folder, open Git Bash and type the following command: "git init". This command initialises a git repository.

4. In the terminal, type the following command that will connect your repo to the remote one that you created before:

    >git remote add origin https://gitlab.com/meriroshi/advanced-css-html_javascriptAl14.git

5. Create a.gitignore file and open it with Notepad.

6. In the gitignore file, copy-paste the following lines:

-    /node_modules
-    package-lock.json


In these lines, we are ignoring from git the node_modules folder and package-lock.json.

7. Add all the folders and files to git by typing the following command: 

    > git add . (Yes there is a dot there, please do not ignore it)

This command will add all folders and files from your directory and extract the ones in gitignore

8. Type the following command: 

    > git commit -m "Initialise test repo"

9. Push the changes to GIT.

    > git push --set-upstream origin master
</details>


How to clone this project? 
<details><summary>Click to expand</summary>
    1. open **git bash** 

    2. cd Desktop (if you want to clone this project in another repository, specify the full path instead of Desktop)
    
    3. git clone https://gitlab.com/meriroshi/advanced-css-html_javascriptAl14.git
</details>

Merge **test** branch into **master** 
<details> <summary>Click to expand</summary>

The following steps are all made from git bash. You can also do these steps by following the user interface from VS Code or Webstorm.


1. git checkout master
2. git pull
3. git checkout test 
4. git pull
5. git merge master 

> (merges master into test)

6. (solve conflict if there any)
7. git push (push the merge and commit resolved conflicts if there any)
8. git checkout master 
9. git merge test

> (merges test into master)

7. git push 

</details>




    
